<?php

echo '*******************************************',"\n";
echo '** Bienvenue dans la console PHP Incolab **',"\n";
echo '*******************************************',"\n \n";

require 'Library/Console/autoload.php';

$console = new Library\Console;

if($console->requete())
{
    if($console->isInstaller())
    {
	$installer = new Library\Console\Installer($console->argument());
	if($installer->isInstallable())
	{
	    echo 'Le module'.$installer->module().' est installable',"\n";
	    echo 'Debut de la copie des sources',"\n";
	    if($installer->installSrc())
	    {
		echo 'Les sources ont été installé avec succès', "\n";
		// Si l'installation c'est bien déroulée, on installe les entities
		echo 'Début de l\'installation des entités',"\n";
		if($installer->installEntities())
		{
		    echo 'Les entités ont été installé avec succès',"\n";
		    // Si l'installation des entities c'est bien déroulée, on installe les modeles
		    echo 'Debut de l\'installation des modèles',"\n";
		    if($installer->installModels())
		    {
			echo 'Les modeles ont été installés avec succès',"\n";
			// Si les modeles sont bien installés, on crée la table dans la bdd
			if($installer->createTable())
			{
			    // On ajoute les parametres dans app.xml
			    echo 'Vous devez ajouter les paramètres à la main pour l\'instant',"\n";
			    if($installer->addRoutes())
			    {
				// On ajoute les Routes
				echo 'Les routes ont été ajoutées',"\n";
				
				if($installer->addConfig())
				{
				    echo "L'installation c'est bien déroulée \n";
				}else{
				    echo "Un problème à eu lieu pendant l'ajout de la Config. \n";
				}
			    }else{
				echo "Un problème à eu lieu pendant l'ajout des routes. \n";
			    }

			}else{
			    // On fera un echo sur la requete SQL
			    
			    echo 'Une erreur à été rencontrée lors de la création de la Table SQL',"\n";
			}
		    }else{
			echo "Un problème à eu lieu pendant l'installation des Modèles \n";
		    }
		}else{
		    echo "Un problème à eu lieu pendant l'installation des entités \n";
		}
	    }else{
		echo "Un problème à eu lieu pendant la copie des sources \n";
	    }
	}else{
	    echo 'Vous semblez vouloir installer le module '.$installer->module().'.',"\n";
	    echo 'Verifiez que le dossier incolab-ifpmod-'.$installer->module().' est présent dans le dossier SRCMods',"\n";
	}
    }else{
	echo "L'unique option disponible actuellement est 'installer'. \n";
    }
}else{
    echo 'Vous devez passer un argument!';
}                    

echo "\n";
echo '*******************************************',"\n";
echo '**     Fin de la Console PHP Incolab     **',"\n";
echo '*******************************************',"\n";
