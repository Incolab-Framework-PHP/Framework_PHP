<?php
// Liens du menu en dur pour l'instant
$menus[0]['titre'] = 'Retour au site';
$menus[0]['lien'] = '/';
$menus[0]['hasDropdown'] = false;
$menus[0]['isDropdown'] = false;
$menus[0]['endDropdown'] = false;
$menus[0]['parent'] = false;

$menus[1]['titre'] = 'Accueil';
$menus[1]['lien'] = '/admin/';
$menus[1]['hasDropdown'] = false;
$menus[1]['isDropdown'] = false;
$menus[1]['endDropdown'] = false;
$menus[1]['parent'] = false;
/*
$menus[2]['titre'] = 'Menu 2';
$menus[2]['lien'] = '#';
$menus[2]['hasDropdown'] = true;
$menus[2]['isDropdown'] = false;
$menus[2]['endDropdown'] = false;
$menus[2]['parent'] = false;

$menus[3]['titre'] = 'Sous-menu 1';
$menus[3]['lien'] = '#';
$menus[3]['hasDropdown'] = false;
$menus[3]['isDropdown'] = true;
$menus[3]['endDropdown'] = false;
$menus[3]['parent'] ='Menu 2';

$menus[4]['titre'] = 'Sous-menu 2';
$menus[4]['lien'] = '#';
$menus[4]['hasDropdown'] = false;
$menus[4]['isDropdown'] = true;
$menus[4]['endDropdown'] = true;
$menus[4]['parent'] = 'Menu 2';

$menus[5]['titre'] = 'Menu 3';
$menus[5]['lien'] = '#';
$menus[5]['hasDropdown'] = false;
$menus[5]['isDropdown'] = false;
$menus[5]['endDropdown'] = false;
$menus[5]['parent'] = false;

$menus[6]['titre'] = 'Menu 4';
$menus[6]['lien'] = '#';
$menus[6]['hasDropdown'] = false;
$menus[6]['isDropdown'] = false;
$menus[6]['endDropdown'] = false;
$menus[6]['parent'] = false;
*/

// Voir l'url de la requete demandé pour la coloration du menu
$request = $this->app->httpRequest(); ?>

<!doctype html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                <?php if(isset($title))
                {
                    echo $title.' |';
                } ?>
                Mon Super Site
            </title>
            <link rel="stylesheet" href="/css/normalize.css">
            <link rel="stylesheet" href="/css/foundation.min.css">
            <script src="/js/vendor/modernizr.js"></script>
        </head>
        
        <body>
            <header>
                <div id="topbar">
                    <nav class="top-bar agrandi" data-topbar >
                        <ul class="left hide-for-small">
                            <li><!-- <img src="/images/Wheels48grey.png" alt="logo" /> --></li>
                        </ul>
                        <ul class="title-area">
                            <li class="name">
                                <h1><a href="/">Mon Site</a></h1>
                            </li>
                            <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
                        </ul>
                        <section class="top-bar-section">
                        <!-- Right Nav Section -->
                            <ul class="right">
                                <li class="divider"></li>
                            <?php
                            foreach($menus as $menu)
                            {   
                                echo '<li class="';
                                // On ajoute "active" sur la page du menu affiché
                                if($request->requestURI() == $menu['lien'])
                                {
                                    echo 'active ';
                                }
                                if($menu['hasDropdown'])
                                {
                                    echo 'has-dropdown"><a href="'.$menu['lien'].'">'.$menu['titre'].'</a><ul class="dropdown">';
                                }else{
                                    echo '"><a href="'.$menu['lien'].'">'.$menu['titre'].'</a></li>';
                                }                               
                                
                                if($menu['endDropdown'])
                                {
                                    echo '</ul></li>';
                                }
                                
                                if($menu['isDropdown'] || !$menu['endDropdown'])
                                {
                            ?>
                                <li class="divider"></li>
                            <?php
                                }
                            } ?>
                            </ul>
                        </section>
                    </nav>
                </div>
            </header>
          <?php if ($user->hasFlash()) echo '<p style="text-align: center;">', $user->getFlash(), '</p>'; ?>
          
          <!-- contenu de la page -->
          <?php echo $content; ?>
          
        <footer class="row marge-top">
            <div class="large-12 columns">
                <hr/>
                <p class="text-center">Copyright Incolab and YottaOctet</p>
            </div>
        </footer>
            
        <script src="/js/vendor/jquery.js"></script>
        <script src="/js/vendor/fastclick.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script>$(document).foundation();</script>
        </body>
    </html>