<?php

namespace Applications\Backend;

class BackendApplication extends \Library\Application
{
    public function __construct()
    {
        parent::__construct();
        
        $this->name = 'Backend';
    }
    
    public function run()
    {
        if(!$this->user->isAuthenticated() || $this->user->isAdmin())
        {
            if($this->user->isAdmin())
            {
                $controller = $this->getController();
            }
            else
            {
                $controller = new Modules\Connexion\ConnexionController($this, 'Connexion', 'index');
            }
            
            $controller->execute();
            
            $this->httpReponse->setPage($controller->page());
            $this->httpReponse->send();
            
        }
        else
        {
            $this->user->setFlash('Emplacement réservé à l\'administrateur');
            // Pour l'instant on redirectionne vers l'index
            $this->httpReponse->redirect('.');
        }
    }
}