<?php

$modules[1]['pathImg'] = '/images/Backend/Accueil/paragraph_justify_2.png';
$modules[1]['titre'] = 'News';
$modules[1]['lien'] = '/admin/news/';

$modules[2]['pathImg'] = '/images/Backend/Accueil/spool.png';
$modules[2]['titre'] = 'Forum';
$modules[2]['lien'] = '/admin/forum/';

$modules[3]['pathImg'] = '/images/Backend/Accueil/image.png';
$modules[3]['titre'] = 'Images';
$modules[3]['lien'] = '/admin/images/';

$modules[4]['pathImg'] = '/images/Backend/Accueil/settings_3.png';
$modules[4]['titre'] = 'Paramètres';
$modules[4]['lien'] = '/admin/parametres/';
?>
<section class="row">
    <header class="large-12 columns">
        <h1 class="text-center">Accueil de l'administration</h1>
    </header>
    <div class="large-12 columns">
        <?php
        if(isset($modules))
        {
            // On parcours $modules qui renvoi le lien vers ladmin du module, licone,
            $imodules = 0;
            foreach($modules as $module)
            {
                if($imodules == 0){ echo '<div class="row">'; }
                ?>
                <div class="large-3 medium-6 columns">
                    <p class="text-center">
                        <a href="<?php echo $module['lien']; ?>">
                            <img src="<?php echo $module['pathImg']; ?>" /><br/>
                            <?php echo $module['titre']; ?>
                        </a>
                    </p>
                </div>
                <?php
                $imodules++;
                if($imodules == 4){ echo '</div>'; }
            }
            if($imodules < 4){ echo '</div>'; }
        }else{
        ?>
        <p class="text-center">Vous devez ajouter des modules pour pouvoir les administrer.</p>
        <?php
        }
        ?>
    </div>
</section>