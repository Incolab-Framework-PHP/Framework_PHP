<?php

namespace Applications\Backend\Modules\Accueil;

class AccueilController extends \Library\BackController
{
    public function executeIndex(\Library\HTTPRequest $request)
    {
        $this->page->addVar('title',"Accueil de l'administration");
        
    }
}