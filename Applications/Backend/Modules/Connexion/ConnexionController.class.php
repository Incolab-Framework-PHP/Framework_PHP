<?php

namespace Applications\Backend\Modules\Connexion;

class ConnexionController extends \Library\BackController
{
    public function executeIndex(\Library\HTTPRequest $request)
    {
        $this->page->addVar('title', 'Connexion');
        
        if($request->postExists('login'))
        {
            $login = $request->postData('login');
            $password = $request->postData('password');
            
            if($login == $this->app->config()->get('login') && $password == $this->app->config()->get('password'))
            {
                $this->app->user()->setAuthenticated(true);
                if($login == $this->app->config()->get('admin'))
                {
                    $this->app->user()->setAdmin(true);
                }
                $this->app->httpReponse()->redirect('.');
            }
        }
    }
}