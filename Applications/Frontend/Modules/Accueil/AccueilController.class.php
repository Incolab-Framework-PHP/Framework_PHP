<?php

namespace Applications\Frontend\Modules\Accueil;

class AccueilController extends \Library\BackController
{
    public function executeIndex(\Library\HTTPRequest $request)
    {
        // On ajoute une définition pour le titre
        $this->page->addVar('title','Bienvenue');
        
        // On récupère le manager que l'on veut ex: /Library/Models/ExempleManager.class.php
        //$manager = $this->managers->getManagerOf('Exemple');
        
        // On recupère l'entités que l'on veut ex: /Library/Entities/Exemple.class.php
        // $entite = new \Library\Entities\Exemple(array());

    }
}