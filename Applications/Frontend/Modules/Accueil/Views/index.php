<?php
if(isset($slideElements))
{ ?>
<!-- Slideshow -->
<div class="row largeG">
    <div class="large-12 columns">
        <ul class="example-orbit hide-for-small" data-orbit data-options="animation:slide;
          pause_on_hover:true;
          resume_on_mouseout:true;
          animation_speed:500;
          navigation_arrows:false;
          bullets:true;
          slide_number:false;
          timer_show_progress_bar:false;">
            <?php
            foreach($slideElements as $element)
            { ?>
            <li <?php if($element['isActive']){ echo 'class="active"'; } ?>>
                <img src="<?php echo $element['image']; ?>" alt="slide <?php echo $element['id']; ?>" />
                <div class="orbit-caption">
                    <div class="row">
                        <div class="blank_400 hide-for-small hide-for-medium"></div>
                        <div class="large-7 medium-8 columns">                            
                            <h3><?php echo $element['titre']; ?></h3>
                            <hr/>
                            <h4 class="vert"><?php echo $element['annonce']; ?></h4>
                        </div>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>    
</div>
<!-- Fin Slideshow -->
<?php
} ?>
<section class="row">
    <article class="large-12 columns">
        <h3 class="text-center">Un Titre d'article</h3>
        <p>Un contenu d'article</p>
        <hr class="hrGreen3"/>
    </article>
</section>