<?php

namespace Library\Console;

class installer
{
    protected $module,
    $nameApp,
    $pathSrc,
    $pathInstall,
    $dao;
    
    public function __construct($module)
    {
	$this->setModule($module);
	$this->setNameApp();
	$this->setPathSrc();
	$this->setPathInstall();
    }
    
    public function isInstallable()
    {
	if(($array = scandir(getcwd().'/SRCMods')) !== false)
	{
	    if(in_array('incolab-ifpmod-'.strtolower($this->module()), $array))
	    {
		return true;
	    }else{
		return false;
	    }
	}else{
	    return false;
	}
    }
    
    public function installSrc()
    {
	// On créé le dossier dans Applications/Modules
	if(!mkdir($this->pathInstall(), 0755))
	{
	    die('Impossible de crééer le dossier');
	}
	
	$dossier = scandir($this->pathSrc.'/Frontend'.$this->module());
        foreach($dossier as $fichier)
        {
            if($fichier !== '.' && $fichier !== '..')
            {
		if(is_dir($this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier))
                {
		    if(!mkdir($this->pathInstall().'/'.$fichier, 0755))
		    {
			die('Impossible de crééer le dossier');
		    }
                    $sDossier = scandir($this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier);
                    foreach($sDossier as $sFichier)
                    {
                        if($sFichier !== '.' && $sFichier !== '..')
                        {
                            copy($this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier.'/'.$sFichier, $this->pathInstall().'/'.$fichier.'/'.$sFichier);
			    echo $this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier.'/'.$sFichier," à été copié. \n";
                        }
                    }
                }else{
                    // On envoie le fichier dans Applications/Frontend/Modules/$mod
		    copy($this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier, $this->pathInstall().'/'.$fichier);
		    echo $this->pathSrc().'/Frontend/'.$this->module().'/'.$fichier," à été copié. \n";
                }
            }
	}
	
	return true;
    }
    
    public function installEntities()
    {
	$dossierSrc = $this->pathSrc().'/Entities';
	$dossierCible = 'Library/Entities';
	if($this->copyDir($dossierSrc, $dossierCible))
	{
	    return true;
	}
    }
    
    public function installModels()
    {
	$dossierSrc = $this->pathSrc().'/Models';
	$dossierCible = 'Library/Models';
	if($this->copyDir($dossierSrc, $dossierCible))
	{
	    return true;
	}
    }
    
    public function createTable()
    {
	$this->setDao();
	if(($modeles = scandir($this->pathSrc().'/SQLModels')) !== false)
	{
	    foreach($modeles as $modele)
	    {
		if($modele !== '.' && $modele !== '..')
		{
		    $modeleSql = $this->pathSrc().'/SQLModels/'.$modele;
		    $sql = file_get_contents($modeleSql);
		    $dao = $this->dao();
		    $dao->query($sql);
		    echo 'La requete: '.$sql.' à bien été ajoutée',"\n";
		}
	        
	    }
	    return true;
	}
    }
    
    public function addRoutes()
    {
	$pathSrcRoutes = $this->pathSrc().'/Config/routes.xml';
	$pathCibleRoutes = getcwd().'/Applications/'.$this->nameApp().'/Config/routes.xml';
	
	$documentXml = new \DOMDocument('1.0','UTF-8');
	$documentXml->formatOutput = true;
	$routesXml = $documentXml->createElement('routes');
	$documentXml->appendChild($routesXml);
	
	// On recupère les anciennes routes
	$xmlOld = new \DomDocument;
	$xmlOld->load($pathCibleRoutes);
	$routesOld = $xmlOld->getElementsByTagName('route');
	
	foreach($routesOld as $routeOld)
	{
	    $newsRoutes = $documentXml->createElement('route');
	    
	    $newsRoutes->setAttribute('url', $routeOld->getAttribute('url'));
	    $newsRoutes->setAttribute('action', $routeOld->getAttribute('action'));
	    $newsRoutes->setAttribute('module', $routeOld->getAttribute('module'));
	    if($routeOld->hasAttribute('vars'))
            {
		$newsRoutes->setAttribute('vars', $routeOld->getAttribute('vars'));
            }
	    
	    $routesXml->appendChild($newsRoutes);
	}
	
	// On recupère les nouvelles routes et on les ajoute à $documentXml
	$xmlSrc = new \DOMDocument;
	$xmlSrc->load($pathSrcRoutes);
	$routesSrc = $xmlSrc->getElementsByTagName('route');
	
	foreach($routesSrc as $routeNews)
	{
	    
	    $newsRoutes = $documentXml->createElement('route');
	    
	    $newsRoutes->setAttribute('url', $routeNews->getAttribute('url'));
	    $newsRoutes->setAttribute('action', $routeNews->getAttribute('action'));
	    $newsRoutes->setAttribute('module', $routeNews->getAttribute('module'));
	    if($routeNews->hasAttribute('vars'))
            {
		$newsRoutes->setAttribute('vars', $routeNews->getAttribute('vars'));
            }
	    
	    $routesXml->appendChild($newsRoutes);
	}
	
	rename(getcwd().'/Applications/'.$this->nameApp().'/Config/routes.xml', getcwd().'/Applications/'.$this->nameApp().'/Config/routes.xml.old');
	
	$handle = fopen(getcwd().'/Applications/'.$this->nameApp().'/Config/routes.xml', 'w+');
	fwrite($handle, $documentXml->saveXML());
	fclose($handle);
	
	echo $documentXml->saveXML();
	
	return true;
	
    }
    
    public function addConfig()
    {
	$pathSrcConfApp = $this->pathSrc().'/'.$this->nameApp().'Config/app.xml';
	if(file_exists($pathSrcConfApp))
	{
	    $pathCibleConfApp = getcwd().'/Applications/'.$this->nameApp().'/Config/app.xml';
	    
	    
	    $documentConfXml = new \DOMDocument('1.0','UTF-8');
	    $documentConfXml->formatOutput = true;
	    $confXml = $documentConfXml->createElement('definitions');
	    $documentConfXml->appendChild($confXml);
	    
	    // On recupère les anciens parametres
	    $xmlOld = new \DomDocument;
	    $xmlOld->load($pathCibleConfApp);
	    $confsOld = $xmlOld->getElementsByTagName('define');
	    
	    foreach($confsOld as $confOld)
	    {
	        $newConfApp = $documentConfXml->createElement('define');
		
	        $newConfApp->setAttribute('var', $confOld->getAttribute('var'));
	        $newConfApp->setAttribute('value', $confOld->getAttribute('value'));
	        
	        $confXml->appendChild($newConfApp);
	    }
	    
	    // On recupère les nouvelles routes et on les ajoute à $documentXml
	    $xmlSrc = new \DOMDocument;
	    $xmlSrc->load($pathSrcConfApp);
	    $confsSrc = $xmlSrc->getElementsByTagName('define');
	    
	    foreach($confsSrc as $confSrc)
	    {
	        
	        $newConfApp = $documentConfXml->createElement('define');
	        
	        $newConfApp->setAttribute('var', $confSrc->getAttribute('var'));
	        $newConfApp->setAttribute('value', $confSrc->getAttribute('value'));
		
		$confXml->appendChild($newConfApp);
	    }
	    
	    rename(getcwd().'/Applications/'.$this->nameApp().'/Config/app.xml', getcwd().'/Applications/'.$this->nameApp().'/Config/app.xml.old');
	    
	    $handle = fopen(getcwd().'/Applications/'.$this->nameApp().'/Config/app.xml', 'w+');
	    fwrite($handle, $documentConfXml->saveXML());
	    fclose($handle);
	    
	    return true;
	}    
    }
    
    public function copyDir($dossierSrc = false, $dossierCible = false, $createCible = false)
    {
	if($dossierSrc != false || $dossierCible != false)
	{
	    if($createCible)
	    {
		// on crée le dossier cible
		if(!mkdir($dossierCible, 0755))
		{
		    die('Impossible de crééer le dossier');
		}
	    }
	    
	    $dossier = scandir($dossierSrc);
	    foreach($dossier as $fichier)
	    {
	        if($fichier !== '.' && $fichier !== '..')
	        {
	            if(is_dir($dossierSrc.'/'.$fichier))
	            {
		        if(!mkdir($dossierCible.'/'.$fichier, 0755))
		        {
			    die('Impossible de crééer le dossier');
		        }
		        $sDossier = scandir($dossierSrc.'/'.$fichier);
			foreach($sDossier as $sFichier)
			{
			    if($sFichier !== '.' && $sFichier !== '..')
			    {
			        copy($dossierSrc.'/'.$fichier.'/'.$sFichier, $dossierCible.'/'.$fichier.'/'.$sFichier);
			        echo $dossierSrc.'/'.$fichier.'/'.$sFichier," à été copié. \n";
			    }
			}
		    }else{
		        // On envoie le fichier dans Applications/Frontend/Modules/$mod
		        copy($dossierSrc.'/'.$fichier, $dossierCible.'/'.$fichier);
			
		        echo $dossierSrc.'/'.$fichier," à été copié. \n";
		    }
		}
	    }
	    
	    return true;
	}else{
	    // Erreur 
	}
    }    
    
    
    // Setters
    
    public function setDao()
    {
	$db = \Library\PDOFactory::getMysqlConnexion();
	$this->dao = $db;
    }
    
    public function setModule($module)
    {
        $this->module = $module;
    }
    
    public function setNameApp()
    {
	$this->nameApp = 'Frontend';
    }
    
    public function setPathSrc()
    {
        $this->pathSrc = getcwd().'/SRCMods/incolab-ifpmod-'.strtolower($this->module());
    }
    
    public function setPathInstall()
    {
        $this->pathInstall = getcwd().'/Applications/Frontend/Modules/'.$this->module();
    }
    
    
    // Getters
    
    public function dao()
    {
	return $this->dao;
    }
    
    public function module()
    {
        return $this->module;
    }
    
    public function nameApp()
    {
	return $this->nameApp;
    }
    
    public function pathSrc()
    {
	return $this->pathSrc;
    }
    
    public function pathInstall()
    {
	return $this->pathInstall;
    }
    
}