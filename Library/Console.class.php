<?php

namespace Library;

class Console
{
    
    protected $requete,
    $module,
    $commande;
    
    public function __construct()
    {
        if($this->isInstaller())
        {
            $this->commande = new \Library\Console\Installer($this->argument());
            // $this->setPathSrcMod($this->argument());
            // $this->setPathInstallMod($this->argument());
        }
    }
    
    public function isInstaller()
    {
        return $this->requete() == 'installer';
    }
    
    public function argv($id)
    {
        if(isset($_SERVER['argv'][$id]))
        {
            return $_SERVER['argv'][$id];
        }
    }
    
    public function requete()
    {
        if(isset($_SERVER['argv'][1]))
        {
            return $_SERVER['argv'][1];
        }
        else{
            return false;
        }
    }
    
    public function argument()
    {
        if(isset($_SERVER['argv'][2]))
        {
            return $_SERVER['argv'][2];
        }else{
        }
    }
    
    public function commande()
    {
        return $this->commande;
    }
    
}