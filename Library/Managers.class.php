<?php

/*  ____________________________________________
 * | Managers                                   |
 * |____________________________________________|
 * | #api: string = null                        |
 * | #dao: object = null                        |
 * | #managers: array = array()                 |
 * |____________________________________________|
 * | +__construct(api:string, dao:object): void |
 * | +getManagersOf(module:string): object      |
 * |____________________________________________|
 */

namespace Library;

class Managers
{
    protected $api = null;
    protected $dao = null;
    protected $managers = array();
    
    public function __construct($api, $dao)
    {
        $this->api = $api;
        $this->dao = $dao;
    }
    
    public function getManagerOf($module)
    {
        if(!is_string($module) || empty($module))
        {
            throw new \InvalidArgumentException('Le module spécifié est invalide');
        }
        
        if(!isset($this->managers[$module]))
        {
            $manager = '\\Library\\Models\\'.$module.'Manager_'.$this->api;
            $this->managers[$module] = new $manager($this->dao);
        }
        
        return $this->managers[$module];
    }
    
    public function getNombreColOf($table)
    {
        $sql ='SELECT COUNT(*) FROM '.$table;
        $requete = $this->dao->prepare($sql);
        //$requete->bindValue(':table', $table);
        $requete->execute();
        $nbOfRows = $requete->fetchColumn();
        $requete->closeCursor();
        return $nbOfRows;
    }
}