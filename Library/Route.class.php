<?php

/*  ___________
 * | Router   |
 * |__________|
 *      |
 *      |__<->_
 *             |
 *             |
 *  ___________|________________________________________________________________
 * | Route                                                                      |
 * |____________________________________________________________________________|
 * | #action: string                                                            |
 * | #module: string                                                            |
 * | #url: string                                                               |
 * | #varsNames: array                                                          |
 * | #vars: array = array()                                                     |
 * |____________________________________________________________________________|
 * | +__construct(url:string,module:string,action:string,varsNames:array): void |
 * | +hasVars(): bool const                                                     |
 * | +match(url:string): bool const                                             |
 * | +setAction(action:string): void                                            |
 * | +setModule(module:string): void                                            |
 * | +setUrl(url:string): void                                                  |
 * | +setVarsNames(varsNames:array): void                                       |
 * | +setVars(vars:array): void                                                 |
 * | +action(): string const                                                    |
 * | +module(): string const                                                    |
 * | +url(): string const                                                       |
 * | +varsNames(): array const                                                  |
 * | +vars(): array const                                                       |
 * |____________________________________________________________________________|
 */

namespace Library;

class Route
{
    protected $action;
    protected $module;
    protected $url;
    protected $varNames;
    protected $vars = array();
    
    public function __construct($url, $module, $action, array $varNames)
    {
        $this->setUrl($url);
        $this->setModule($module);
        $this->setAction($action);
        $this->setVarsNames($varNames);
    }
    
    public function hasVars()
    {
        return !empty($this->varNames);
    }
    
    public function match($url)
    {
        if(preg_match('#^'.$this->url.'$#', $url, $matches))
        {
            return $matches;
        }
        else
        {
            return false;
        }
    }
    
    // Setters
    
    public function setAction($action)
    {
        if(is_string($action))
        {
            $this->action = $action;
        }
    }
    
    public function setModule($module)
    {
        if(is_string($module))
        {
            $this->module = $module;
        }
    }
    
    public function setUrl($url)
    {
        if(is_string($url))
        {
            $this->url = $url;
        }
    }
    
    public function setVarsNames(array $varsNames)
    {
        $this->varNames = $varsNames;
    }
    
    public function setVars(array $vars)
    {
        $this->vars = $vars;
    }
    
    // Getters
    
    public function action()
    {
        return $this->action;
    }
    
    public function module()
    {
        return $this->module;
    }
    
    public function vars()
    {
        return $this->vars;
    }
    
    public function varsNames()
    {
        return $this->varNames;
    }
}