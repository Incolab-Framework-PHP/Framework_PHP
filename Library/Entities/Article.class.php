<?php

namespace Library\Entities;

class Article extends \Library\Entity
{
    protected $auteur,
    $titre,
    $alias,
    $contenu,
    $categorie,
    //$tag,
    $dateAjout,
    $dateModif;
    
    const AUTEUR_INVALIDE = 1;
    const TITRE_INVALIDE = 2;
    const ALIAS_INVALIDE = 3;
    const CONTENU_INVALIDE = 4;
    const CATEGORIE_INVALIDE = 5;
    
    public function isValid()
    {
        return !(empty($this->auteur) || empty($this->titre) || empty($this->contenu));
    }
    
    // SETTERS
    
    public function setAuteur($auteur)
    {
        if(!is_string($auteur) || empty($auteur))
        {
            $this->erreurs[] = self::AUTEUR_INVALIDE;
        }
        else
        {
            $this->auteur = $auteur;
        }
    }
    
    public function setTitre($titre)
    {
        if(!is_string($titre) || empty($titre))
        {
            $this->erreurs[] = self::TITRE_INVALIDE;
        }
        else
        {
            $this->titre = $titre;
        }
    }
    
    public function setAlias($alias)
    {
        if(!is_string($alias))
        {
            $this->erreurs[] = self::ALIAS_INVALIDE;
        }
        else
        {
            $this->alias = $alias;
        }
    }
    
    public function setContenu($contenu)
    {
        if(!is_string($contenu) || empty($contenu))
        {
            $this->erreurs[] = self::CONTENU_INVALIDE;
        }
        else
        {
            $this->contenu = $contenu;
        }
    }
    
    public function setCategorie($categorie)
    {
        if(!is_string($categorie))
        {
            $this->erreurs[] = self::CATEGORIE_INVALIDE;
        }
        else
        {
            $this->categorie = $categorie;
        }
    }
    
    public function setDateAjout(\DateTime $dateAjout)
    {
        $this->dateAjout = $dateAjout;
    }
    
    public function setDateModif(\DateTime $dateModif)
    {
        $this->dateModif = $dateModif;
    }
    
    // Getters
    
    public function auteur()
    {
        return $this->auteur;
    }
    
    public function titre()
    {
        return $this->titre;
    }
    
    public function alias()
    {
        return $this->alias;
    }
    
    public function contenu()
    {
        return $this->contenu;
    }
    
    public function categorie()
    {
        return $this->categorie;
    }
    
    public function dateAjout()
    {
        return $this->dateAjout;
    }
    
    public function dateModif()
    {
        return $this->dateModif;
    }
    
}