<?php

namespace Library\Entities;

class Image extends Library\Entity
{
    protected $nom,
    $categorie,
    $alt,
    $path;
    
    const NOM_INVALIDE = 1;
    const CATEGORIE_INVALIDE = 2;
    const ALT_INVALIDE = 3;
    const PATH_INVALIDE = 4;
    
    
    
    public function isValid()
    {
        return !(empty($this->nom) || empty($this->alt) || empty($this->path));
    }
    
    
    //Setters
    
    public function setNom($nom)
    {
        if(empty($nom) || !is_string($nom))
        {
            $this->erreurs[] = self::NOM_INVALIDE;
        }else{
            $this->nom = $nom;
        }
    }
    
    public function setCategorie($categorie)
    {
        if(empty($categorie) || !is_string($categorie))
        {
            $this->erreurs[] = self::CATEGORIE_INVALIDE;
        }else{
            $this->categorie = $categorie;
        }
    }
    
    public function setAlt($alt)
    {
        if(empty($alt) || !is_string($alt))
        {
            $this->erreurs[] = self::ALT_INVALIDE;
        }else{
            $$this->alt = $alt;
        }
    }
    
    public function setPath($path)
    {
        if(empty($path) || !is_string($path))
        {
            $this->erreurs[] = self::PATH_INVALIDE;
        }
    }
    
    
    // Getters
    
    public function nom()
    {
        return $this->nom;
    }
    
    public function categorie()
    {
        return $this->categorie;
    }
    
    public function alt()
    {
        return $this->alt;
    }
    
    public function path()
    {
        return $this->path;
    }
}