<?php

namespace Library\Entities;

class Slideshow extends \Library\Entity
{
    protected $image,
    $titre,
    $annonce,
    $isActive;
    
    const IMAGE_INVALIDE = 1;
    const TITRE_INVALIDE = 2;
    const ANNONCE_INVALIDE = 3;
    const ISACTIVE_INVALIDE = 4;
    
    public function isValid()
    {
        return !(empty($this->titre) || empty($this->titre) || empty($this->annonce) || empty($this->isActive));
    }
    
    // SETTERS
    
    public function setImage($image)
    {
        if(!is_string($image))
        {
            $this->erreurs[] = self::IMAGE_INVALIDE;
        }
        else
        {
            $this->image = $image;
        }
    }
    
    public function setTitre($titre)
    {
        if(!is_string($titre))
        {
            $this->erreurs[] = self::TITRE_INVALIDE;
        }
        else
        {
            $this->titre = $titre;
        }
    }
    
    public function setAnnonce($annonce)
    {
        if(!is_string($annonce))
        {
            $this->erreurs[] = self::ANNONCE_INVALIDE;
        }
        else
        {
            $this->annonce = $annonce;
        }
    }
    
    public function setIsActive($isActive)
    {
        if(!is_bool($isActive))
        {
            $this->erreurs[] = self::ISACTIVE_INVALIDE;
        }
        else
        {
            $this->isActive = $isActive;
        }
    }
    
    // GETTERS
    
    public function image()
    {
        return $this->image;
    }
    
    public function titre()
    {
        return $this->titre;
    }
    
    public function annonce()
    {
        return $this->annonce;
    }
    
    public function isActive()
    {
        return $this->isActive;
    }
    
    
}