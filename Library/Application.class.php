<?php

/*  _________________________________
 * | Application                    |
 * |________________________________|
 * | #httpRequest: HTTPRequest      |
 * | #httpReponse: HTPPReponse      |
 * | #name: string                  |
 * |________________________________|
 * | +__construct(): void           |
 * | +getController()               |
 * | +run(): void                   |
 * | +httpRequest(): HTTPRequest    |
 * | +httpReponse(): HTTPReponse    |
 * | +name(): string                |
 * |________________________________|
 */

namespace Library;

abstract class Application
{
    protected $httpRequest;
    protected $httpReponse;
    protected $name;
    protected $user;
    protected $config;
    
    public function __construct()
    {
        $this->httpRequest = new HTTPRequest($this);
        $this->httpReponse = new HTTPReponse($this);
        $this->name = '';
        $this->user = new User($this);
        $this->config = new Config($this);
    }
    
    public function getController()
    {
        $router = new \Library\Router;
        $xml = new \DOMDocument;
        $xml->load(__DIR__.'/../Applications/'.$this->name.'/Config/routes.xml');
        
        $routes = $xml->getElementsByTagName('route');
        
        // On parcours les route du fichier XML
        foreach($routes as $route)
        {
            $vars = array();
            
            // On regarde si des variables sont présentes dans l'url
            if($route->hasAttribute('vars'))
            {
                $vars = explode(',',$route->getAttribute('vars'));
            }
            
            // On ajoute la route au routeur
            $router->addRoute(new Route($route->getAttribute('url'), $route->getAttribute('module'), $route->getAttribute('action'), $vars));
        }
        
        try
        {
            // On récupère la route correspondant à l'URL
            $matchedRoute = $router->getRoute($this->httpRequest->RequestURI());
        }
        catch(\RuntimeException $e)
        {
            if($e->getCode() == \Library\Router::NO_ROUTE)
            {
                // Si aucune route de correspond, c'est que la page n'existe pas
                $this->httpReponse->redirect404();
            }
        }
        
        // On ajoute les variables de l'URL au tableau $_GET
        $_GET = array_merge($_GET, $matchedRoute->vars());
        
        // On instancie le controlleur
        $controllerClass = 'Applications\\'.$this->name.'\\Modules\\'.$matchedRoute->module().'\\'.$matchedRoute->module().'Controller';
        return new $controllerClass($this, $matchedRoute->module(), $matchedRoute->action());
    }
    
    abstract public function run();
    
    public function httpRequest()
    {
        return $this->httpRequest;
    }
    
    public function httpReponse()
    {
        return $this->httpReponse;
    }
    
    public function name()
    {
        return $this->name;
    }
    
    public function user()
    {
        return $this->user;
    }
    
    public function config()
    {
        return $this->config;
    }
}