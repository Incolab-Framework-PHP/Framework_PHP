<?php

namespace Library\Models;

use Library\Entities\Slideshow;

class SlideshowManager_PDO extends SlideshowManager
{
    public function getList()
    {
        $sql = 'SELECT id, image, titre, annonce, isActive FROM slideshows ORDER BY id DESC';
        
        $requete = $this->dao->query($sql);
        
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\Slideshow');
        
        $slideElements = $requete->fetchAll();
        
        $requete->closeCursor();
        
        return $slideElements;
    }
}