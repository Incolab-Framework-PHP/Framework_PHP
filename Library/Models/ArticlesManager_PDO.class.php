<?php

namespace Library\Models;

use Library\Entities\News;

class ArticlesManager_PDO extends ArticlesManager
{
    public function getList()
    {
        $sql = 'SELECT id, auteur, titre, alias, contenu, categorie, dateAjout, dateModif FROM services ORDER BY id DESC';
        
        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\Articles');
        
        $listeNews = $requete->fetchAll();
        
        foreach ($listeNews as $news)
        {
            $news->setDateAjout(new \DateTime($news->dateAjout()));
            $news->setDateModif(new \DateTime($news->dateModif()));
        }
        
        $requete->closeCursor();
        
        return $listeNews;
    }
}