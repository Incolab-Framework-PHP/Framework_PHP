<?php

namespace Library\Models;

abstract class ImageManager
{
     /**
   * Méthode retournant une liste d'images demandée
   * @param $debut int La première image à sélectionner
   * @param $limite int Le nombre de image à sélectionner
   * @return array La liste des images. Chaque entrée est une instance d'Image.
   */
    abstract public function getList($debut = -1, $limite = -1);
    
      /**
   * Méthode retournant une liste d'images de la categorie demandée
   * @param $categorie La categorie des imes a selectionner
   * @param $debut int La première image à sélectionner
   * @param $limite int Le nombre de image à sélectionner
   * @return array La liste des images. Chaque entrée est une instance d'Image.
   */
    abstract public function getListOf($categorie, $debut = -1, $limite = -1);
    
}
