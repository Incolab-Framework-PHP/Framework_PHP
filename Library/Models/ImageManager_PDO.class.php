<?php

namespace Library\Models;

use Library\Entities\Image;

class ImageManager_PDO extends ImageManager
{
    public function getList($debut = -1, $limite = -1)
    {
        $sql = 'SELECT id, nom, categorie, alt, path FROM image ORDER BY id DESC';
        
        if($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT '.(int) $limite.' OFFSET '.(int) $debut;
        }
        
        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\Image');
        
        $images = $requete->fetchAll();
        
        $requete->closeCursor();
        
        return $images;
    }
    
    public function getListOf($categorie, $debut = -1, $limite = -1)
    {
        $sql = 'SELECT id, nom, alt, path FROM image ORDER BY id DESC WHERE categorie = :categorie';
        
        if($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT '.(int) $limite.' OFFSET '.(int) $debut;
        }
        
        $requete = $this->dao->prepare($sql);
        
        $requete->bindValue(':categorie', $categorie);
        $requete->execute();
        
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\Image');
        
        $images = $requete->fetchAll();
        
        $requete->closeCursor();
        
        return $images;
    }
    
}