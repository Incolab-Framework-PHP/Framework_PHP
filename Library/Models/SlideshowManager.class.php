<?php

namespace Library\Models;

abstract class SlideshowManager extends \Library\Manager
{
        /**
   * Méthode retournant une liste d'elements de slideshow demandée
   * @return array La liste des news. Chaque entrée est une instance de Slideshow.
   */
        
    abstract public function getList();
}