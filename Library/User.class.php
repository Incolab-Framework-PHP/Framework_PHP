<?php

/*  ______________________
 * | ApplicationComponent |
 * |______________________|
 *      |
 *      |__<-__
 *             |
 *  ___________|______________________________________
 * | User                                             |
 * |__________________________________________________|
 * | +getAttribute(attr:mixed): mixed                 |
 * | +getFlash(): string                              |
 * | +hasFlash(): bool                                |
 * | +isAuthenticated(): bool                         |
 * | +setAttribute(attr:mixed, value:mixed): void     |
 * | +setAuthenticated(authenticated:bool=true): void |
 * | +setFlash(value:string): void                    |
 * |__________________________________________________|
 */

namespace Library;

session_start();

class User extends ApplicationComponent
{
    
    public function logout()
    {
        unset($_SESSION['auth']);
        
        if(isset($_SESSION['admin']))
        {
            unset($_SESSION['admin']);
        }
        
        if(isset($_SESSION['pseudo']))
        {
            unset($_SESSION['pseudo']);
        }
        
        if(isset($_SESSION['userId']))
        {
            unset($_SESSION['userId']);
        }
    }
    
// Getters
    public function getAttribute($attr)
    {
        return isset($_SESSION[$attr]) ? $_SESSION[$attr] : null;
    }
    
    public function getFlash()
    {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        
        return $flash;
    }
    
    public function hasFlash()
    {
        return isset($_SESSION['flash']);
    }
    
    public function isAuthenticated()
    {
        return isset($_SESSION['auth']) && $_SESSION['auth'] === true;
    }
    
    public function isAdmin()
    {
        return isset($_SESSION['admin']) && $_SESSION['admin'] === true;
    }
    
    public function getPseudo()
    {
        return $_SESSION['pseudo'];
    }
    
    public function getPrivs()
    {
        if(isset($_SESSION['privs']))
        {
            return $_SESSION['privs'];
        }else{
            return array('aucun');
        }
    }
    
    public function getUserId()
    {
        return $_SESSION['userId'];
    }
    

// Setters
    public function setAttribute($attr, $value)
    {
        $_SESSION[$attr] = $value;
    }
    
    public function setAuthenticated($authenticated = true)
    {
        if(!is_bool($authenticated))
        {
            throw new \InvalidArgumentException('La valeur spécifiée à la mathode User::setAuthenticated() doit être un boolean');
        }
        
        $_SESSION['auth'] = $authenticated;
    }
    
    public function setAdmin($admin = true)
    {
        if(!is_bool($admin))
        {
            throw new \InvalidArgumentException('La valeur spécifiée à la mathode User::setAdmin() doit être un boolean');
        }
        
        $_SESSION['admin'] = $admin;
    }
    
    public function setFlash($value)
    {
        $_SESSION['flash'] = $value;
    }
    
    public function setPseudo($pseudo)
    {
        $_SESSION['pseudo'] = $pseudo;
    }
    
    public function setPrivs($privs)
    {
        $_SESSION['privs'] = $privs;
    }
    
    public function setUserId($userId)
    {
        $_SESSION['userId'] = (int)$userId;
    }
    
}
