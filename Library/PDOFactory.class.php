<?php

namespace Library;

class PDOFactory
{
    public static function getMysqlConnexion()
    {
        $options = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
        
        $db = new \PDO('mysql:host=localhost;dbname=dbname', 'user', 'password', $options);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        
        return $db;
    }
}