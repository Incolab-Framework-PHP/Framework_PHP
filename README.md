Incolab Framework PHP
=====================
### Framework PHP léger en Français.

## Configuration Apache
Le virtualhost d'Apache doit pointer dans le dossier Web du framework.
Le mod "rewrite" d'Apache doit être activé. Sur Debian: `# a2enmod rewrite`.

## Configuration de la BDD
Les paramètres de la base de données se configurent dans le fichier Library/PDOFactory.class.php

## La console
Pour utiliser la console en PHP CLI vous devez vous trouver à la racine de votre projet.
`$ php Applications/console.php`

### Installation d'un module
Exemple avec le module News
Pour installer un module présent dans le depôt Incolab, vous devez le télécharger dans le dossier SRCMods:
`user@machine:/var/www/votre_projet$ cd SRCMods/`
`user@machine:/var/www/votre_projet/SRCMods$ wget https://gitorious.org/incolab/ifpmod-news/archive/9b1830a2b6956b74d9bdf5a695017f5115d6f7bb.tar.gz -O master.tar.gz`
`user@machine:/var/www/votre_projet/SRCMods$ tar -xvf master.tar.gz`

Ensuite, vous devez utiliser la console pour ajouter le module à votre projet:
`user@machine:/var/www/votre_projet/SRCMods$ cd ../`
`user@machine:/var/www/votre_projet$ php Application/console.php installer News`

Votre Module est maintenant installé.
